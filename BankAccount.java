
package bankaccount;

/**
 *
 * @author Geanina Tambaliuc
 */
public abstract class BankAccount {
    protected int accnumber,ssn;
    protected double balance;
    protected String name;
    
    public BankAccount(int accnumber,int ssn, String name)
    {
        this.accnumber=accnumber;
        this.ssn=ssn;
        this.balance=0;
        this.name=name;
    }
    public abstract void deposit(double amount);
    public abstract void withdraw(double amount);
    public double getBalance()
    {
        return balance;
    }
    public String toString()
    {
        String str="Name: "+this.name+" Account Number: "+this.accnumber+" Social Security Number: ";
        str=str+this.ssn+" Balance: "+this.getBalance();
        return str;
            
    }
    public boolean equals(Object object)
    {
        if(object instanceof BankAccount)
        {
            BankAccount other=(BankAccount) object;
           return this.accnumber==other.accnumber;
        }
        else return false;
    }
    public String getName()
    {
        return name;
    }
    public int getAcNum()
    {
        return accnumber;
    }
    public int getSocial()
    {
        return ssn;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void setAcNum(int accnumber)
    {
        this.accnumber=accnumber;
    }
    public void setSocial(int ssn)
    {
        this.ssn=ssn;
    }

    
    
}
