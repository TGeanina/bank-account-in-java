
package bankaccount;

/**
 *
 * @author Geanina Tambaliuc
 */
public class CheckTransaction extends BankAccount{
    protected int number;
    protected String date;
    protected double payee, amount;
    
    public CheckTransaction(int accnumber,int ssn, String name)
    {
        super(accnumber,ssn,name);
    }
    public CheckTransaction(int accnumber,int ssn, String name,int number, String date,
            double payee, double amount)
    {
        super(accnumber,ssn,name);
        this.number=number;
        this.date=date;
        this.payee=payee;
        this.amount=amount;
            
    }
    @Override
    public void deposit(double amount)
    {
        this.balance=this.balance+amount;
    }
     @Override
    public void withdraw(double amount)
    {
        this.balance=this.balance-amount;
    }
    public int getNumber()
    {
        return number;
    }
    public String getDate()
    {
        return date;
    }
    public double getPayee()
    {
        return payee;
    }
    public double getAmount()
    {
        return amount;
    }
    public void setNumber(int n)
    {
        this.number=n;
    }
    public void setDate(String d)
    {
        this.date=d;
    }
    public void setPayee(double p)
    {
        this.payee=p;
    }
    public void setAmount(double amount)
            
    {
        this.amount=amount;
    }
}
