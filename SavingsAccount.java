
package bankaccount;

/**
 *
 * @author Geanina Tambaliuc
 */
public class SavingsAccount extends BankAccount{
    protected double interestRate;
    public SavingsAccount(int accnumber,int ssn, String name,double rate)
    {
        super(accnumber,ssn,name);
        this.interestRate=rate;
    }
    public void addInterest()
    {
        double interest=this.getBalance()*this.interestRate/100;
        deposit(interest);
    }
    @Override
    public void deposit(double interest)
    {
        
        this.balance=balance+interest;
    }
    @Override
    public void withdraw(double amount)
    {
        this.balance=this.balance-amount;
    }
    @Override
    public String toString()
    {
        return super.toString()+" Account Type: Savings";
    }
}
