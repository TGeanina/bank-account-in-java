
package bankaccount;

/**
 *
 * @author Geanina Tambaliuc
 */
import java.util.ArrayList;
public abstract class CheckingAccount extends BankAccount{
    ArrayList<CheckingAccount> CheckTransaction= new ArrayList<CheckingAccount>();
    
    
    public CheckingAccount(int accnumber,int ssn, String name)
    {
        super(accnumber,ssn,name);
        
    }
    
    @Override
    public void deposit(double amount)
    {
        this.balance=this.balance+amount;
    }

    /**
     *
     */
    
    public void withdraw( CheckingAccount myCheck)
    {
        this.balance=this.balance-myCheck.balance;
        CheckTransaction.add(myCheck);
    }
    @Override
    public String toString()
    {
        return super.toString()+" Account Type: Checking ";
    }
}
